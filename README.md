## How to use my-awesome-app
 - composer install
 - cp .env.example .env
 - fill out .env with your database information
 - php artisan key:generate 
 - php artisan migrate:fresh --seed
 - php artisan jwt:secret
 - npm install
 - npm run dev
 - php artisan serve

test login: user: test@test.com pass: test

## How to run tests:
 - fill out .env.testing with your database information
 - php artisan key:generate
 - php vendor/bin/phpunit
 