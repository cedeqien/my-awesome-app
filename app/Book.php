<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description',
    ];

    /**
     * Method used to obtain multiple pdfs linked to the book.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne the relation with pdf model
     */
    public function pdfs()
    {
        return $this->hasMany('App\Pdf');
    }
}
