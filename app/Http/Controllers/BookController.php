<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use App\Http\Requests\BookRequest;
use App\Book;
use App\Pdf;

class BookController extends Controller
{
    /**
     * Get a list of book. (Cached for faster requests)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $books = Cache::remember('books', 2880, function () {
            return Book::with('pdfs')->orderBy('created_at', 'desc')->get();
        });

        return response()->json($books, 200);
    }

    /**
     * Store a book inside the Database.
     *
     * @param  \Illuminate\Http\BookRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BookRequest $request)
    {
        $book = Book::create($request->all());

        if ($request->hasfile('files')) {
            foreach ($request->file('files') as $file) {
                $pdf = new Pdf();
                $pdf->name = $file->hashName();
                $pdf->book_id = $book->id;
                $pdf->save();

                $file->store('public/books');
            }
        }

        Cache::forget('books');

        return response()->json($book, 201);
    }

    /**
     * Show a book from the database with an id.
     *
     * @param  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $book = Book::findorFail($id);

        return response()->json($book);
    }

    /**
     * Update a book from the Database.
     *
     * @param  \Illuminate\Http\BookRequest  $request
     * @param  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BookRequest $request, $id)
    {
        $book = Book::findorFail($id);
        Cache::forget('books');

        $book->update($request->all());

        return response()->json($book, 200);
    }

    /**
     * Delete a book inside the Database.
     *
     * @param  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Book::destroy($id);
        Cache::forget('books');

        return response()->json(null, 204);
    }
}
