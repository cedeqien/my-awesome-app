<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Get a list of book. (Cached for faster requests)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = Cache::remember('users', 2880, function () {
            return User::all();
        });

        return response()->json($users, 200);
    }
}
