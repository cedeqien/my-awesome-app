
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import {routes} from './routes.js';
import App from './App.vue';
import store from './store';
import Vue from 'vue';


const token = localStorage.getItem('token');
if (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}
axios.defaults.baseURL = '/';
window.Vue = Vue;

Vue.use(Vuetify);
Vue.use(VueRouter);

import 'vuetify/dist/vuetify.min.css';

const router = new VueRouter({
  routes: routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem('token') == null) {
      next({name: 'login'});
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem('token') == null) {
      next();
    } else {
      next({name: 'home'});
    }
  } else {
    next();
  }
});

const app = new Vue({ // eslint-disable-line
  el: '#app',
  components: {App},
  template: '<App/>',
  router: router,
  store,
});
