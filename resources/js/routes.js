import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import Books from './views/Books.vue';
import Book from './views/Book.vue';
import BookCU from './views/BookCU.vue';
import Users from './views/Users.vue';

const routes = [
  {
    path: '/',
    component: Home,
    name: 'home',
  },
  {
    path: '/login',
    component: Login,
    name: 'login',
    meta: {
      guest: true,
    },
  },
  {
    path: '/register',
    component: Register,
    name: 'register',
    meta: {
      guest: true,
    },
  },
  {
    path: '/books',
    component: Books,
    name: 'books',
  },
  {
    path: '/users',
    component: Users,
    name: 'users',
  },
  {
    path: '/book/edit/:id',
    component: BookCU,
    name: 'book-edit',
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/book/create',
    component: BookCU,
    name: 'book-create',
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/book/:id',
    component: Book,
    name: 'book',
  },
];

export {routes};
