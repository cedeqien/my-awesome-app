<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Book;

class BookTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Integration test on 'store' method from BookController.
     *
     * @return void
     */
    public function testAddBook()
    {
        $bookName = 'good test';

        $data = [
            'title' => $bookName,
            'description' => 'lorem bla'
        ];

        $this->post('/api/books', $data)->assertStatus(201)->assertJson($data);

        $this->assertDatabaseHas('books', [
            'title' => $bookName
        ]);
    }

    /**
     * Integration test on 'index' method from BookController.
     *
     * @return void
     */
    public function testGetBooks()
    {
        $this->get('/api/books')
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [ 'id', 'title', 'description' ],
            ]);
    }

    /**
     * Integration test on 'update' method from BookController.
     *
     * @return void
     */
    public function testUpdateBooks()
    {
        $bookName = 'changed test';

        $book = Book::where('title', 'good test')->first();

        $this->put("/api/books/$book->id", [
            'title' => $bookName,
            'description' => 'testest'
        ]);

        $this->assertDatabaseHas('books', [
            'title' => $bookName
        ]);
    }

    /**
     * Integration test on 'show' method from BookController.
     *
     * @return void
     */
    public function testShowBooks()
    {
        $book = factory(Book::class)->create();
        
        $test = $this->get('/api/books/', [$book->id]);

        $test->assertStatus(200);
    }
}
